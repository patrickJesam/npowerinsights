import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import * as firebase from 'firebase';

@Component({
    selector: 'app-feeds',
    templateUrl: './feeds.component.html',
    styleUrls: ['./feeds.component.css']
})
export class FeedsComponent implements OnInit {
    feeds: any;
    activateLoader: boolean;
    singleFeed: any;
    previousCount: number;
    nextCount: number;
    feedsClone: any;

    constructor(private database: AngularFireDatabase) {
        this.activateLoader = true;
        this.getFeeds();
    }

    ngOnInit() {
    }

    async getFeeds(){
        this.database.list('/feeds')
        .valueChanges()
        .subscribe(data => {
            console.log(data);
            let feedsData = data.reverse();
            this.feedsClone = feedsData;
            this.feeds = feedsData.slice(0, 10);
            this.nextCount = 10;
            this.previousCount = 0;
            this.activateLoader = false;
        });
    }

    delete(data){
        console.log(data);
        const feeds = this.database.object('/feeds/'+data.title);
        feeds.remove().then(data => {
            console.log(data)
        });
    }

    previous(){
        this.feeds = this.feedsClone.slice(this.previousCount-10, this.previousCount);
        console.log(this.nextCount);console.log(this.previousCount);
        //this.previousCount = this.nextCount-10;
    }

    next(){
        this.feeds = this.feedsClone.slice(this.nextCount, this.nextCount+10);
        this.nextCount = this.nextCount+10;
        this.previousCount = this.nextCount-10;
        console.log(this.nextCount);console.log(this.previousCount);
    }

    handleClick(data){
        console.log(data);
    }

    show(data){
        console.log(data);
        this.singleFeed = data;
    }

    guid() {
        function s4() {
          return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }
}
