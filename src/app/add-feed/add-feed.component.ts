import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, ReactiveFormsModule, FormsModule } from "@angular/forms";
import { FlashMessagesService } from 'angular2-flash-messages';
import { AngularFireDatabase } from 'angularfire2/database';
import * as firebase from 'firebase';
import { Headers, Http } from '@angular/http';

@Component({
    selector: 'app-add-feed',
    templateUrl: './add-feed.component.html',
    styleUrls: ['./add-feed.component.css']
})
export class AddFeedComponent implements OnInit {
    myForm: FormGroup;
    imageUrl: string;
    activateLoader: boolean;
    file: any;
    states: any;

    constructor(private fb: FormBuilder, private database: AngularFireDatabase, private flashMessage: FlashMessagesService, private http: Http) {
        this.getStates();
     }

    ngOnInit() {
        this.formMethods();
    }

    private formMethods() {
        this.myForm = this.fb.group({
            title: ['', Validators.required],
            body: ['', Validators.required],
            ppa: ['', Validators.required],
            state: ['', Validators.required],
            program: ['', Validators.required]
        });
    }

    public async getStates(){
        return await this.http.get('https://api.admin.npvn.ng/states')
        .map(res => res.json())
        .subscribe(data => {
          console.log(data.data);
          this.states = data.data;
        },
        error => {
          console.log("Oops! An error occured please check that you are connected to the internet...");
        });
    }

    public makeid() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
      
        for (var i = 0; i < 20; i++)
          text += possible.charAt(Math.floor(Math.random() * possible.length));
      
        return text;
    }

    config: Object = {
        placeholderText: 'Enter feed body'
    }

    async submit(values){
        console.log(values);
        this.activateLoader = true;
        //read file to be uploaded
        let reader = new FileReader();
        let file = this.file.target.files[0];
        reader.readAsDataURL(file);
        var storage = firebase.storage().ref('/images');
        await storage.child(this.makeid()).put(file).then(
            data => {
                this.imageUrl = data.downloadURL;
            }
        );
        console.log(this.imageUrl);
        // upload other form fields
        if(this.imageUrl){
            const toSend = this.database.object(`/feeds/${values.title}`);
            toSend.set({
                title: values.title,
                body: values.body,
                date: new Date().toDateString(),
                imageUrl: this.imageUrl,
                ppa: values.ppa,
                state: values.state,
                program: values.program
            });

            this.flashMessage.show('Your feed item was \n uploaded successfully!', { cssClass: 'alert-success', timeout: 3000 });
            this.activateLoader = false;
            this.formMethods();
        }else{
            this.flashMessage.show('An error occured, please check \n that you are connected to the internet', { cssClass: 'alert-success', timeout: 3000 });
        }
    }

    handleUpload(file){
        this.file = file;
        //console.log(this.file);
    }
}
