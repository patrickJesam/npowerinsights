import { RouterModule, Routes } from '@angular/router';
import { ModuleWithProviders } from '@angular/core/src/metadata/ng_module';
import { NotFoundComponent } from './not-found/not-found.component';
import { AddFeedComponent } from './add-feed/add-feed.component';
import { FeedsComponent } from './feeds/feeds.component';

export const AppRoutes: Routes = [
    { path: '', redirectTo: 'add', pathMatch: 'full' },
    { path: 'add', component: AddFeedComponent },
    { path: 'feeds', component: FeedsComponent },
    { path: '**', component: NotFoundComponent }
    // { path: 'complain/:id', component: ComplainItem }
];
	
export const ROUTING: ModuleWithProviders = RouterModule.forRoot(AppRoutes); 