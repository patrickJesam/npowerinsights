import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { AddFeedComponent } from './add-feed/add-feed.component';
import { FeedsComponent } from './feeds/feeds.component';
import { FlashMessagesModule } from 'angular2-flash-messages';

// Import the AF2 Module
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NotFoundComponent } from './not-found/not-found.component';
import { ROUTING } from './app.routing';
import { EditorModule } from '@tinymce/tinymce-angular';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { HttpModule } from '@angular/http';

export const FIREBASE_CONFIG = {
    apiKey: "AIzaSyBlSs7anQB598un7w1BEvzGWaHhgN1WnSw",
    authDomain: "ovpfeeds.firebaseapp.com",
    databaseURL: "https://ovpfeeds.firebaseio.com",
    projectId: "ovpfeeds",
    storageBucket: "ovpfeeds.appspot.com",
    messagingSenderId: "150506523907"
};

export const AppRoutes: Routes = [
    { path: '', redirectTo: 'add', pathMatch: 'full' },
    { path: 'add', component: AddFeedComponent },
    { path: 'feeds', component: FeedsComponent },
    { path: '**', component: NotFoundComponent }
    // { path: 'complain/:id', component: ComplainItem }
];

@NgModule({
    declarations: [
        AppComponent,
        AddFeedComponent,
        FeedsComponent,
        NotFoundComponent
    ],
    imports: [
        BrowserModule,
        RouterModule.forRoot(AppRoutes),
        FlashMessagesModule.forRoot(),
        AngularFireModule.initializeApp(FIREBASE_CONFIG),
        AngularFireDatabaseModule,
        ReactiveFormsModule,
        EditorModule,
        FroalaEditorModule.forRoot(), 
        FroalaViewModule.forRoot(),
        HttpModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
